const { raw } = require('body-parser');
const express       = require('express')
const app           = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true}));

app.get('/:data', (req, res) => {
    
    if(!(JSON.parse(req.params.data)[0])) {
        res.send("Data not found")
    }

    const row_data = JSON.parse(req.params.data)
    let first_row = row_data[0]
    let h = []
    let d = []
    const total_key = Object.keys(row_data[0]).length
    
    for(let i = 0; i < total_key; i++) {
        h.push(Object.keys(first_row)[i])
    }

    for (let i =0; i < row_data.length; i++) {
        let row = []
        for(const value of h) {
            row.push(row_data[i][value])
        }
        d.push(row)
    }
    let result = {"h":h,"d":d}

    return res.status(200).send(result);
})

app.listen(3000, () => {
    console.log("running on port 3000")
})