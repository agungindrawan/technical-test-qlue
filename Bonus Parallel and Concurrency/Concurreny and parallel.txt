Concurrency
-   We can describe concurrency as a job that is done one by one, concurrency is likened to an 
    interaction between a customer service and a bank customer, a bank customer needs to take a 
    ticket that has a ticket number that will be served by a customer service in sequence.

Parallel
-   Parallel can be described as a restaurant guest against several chefs in the kitchen,
    a guest waiter records all the customer orders and then sends it to several chefs in 
    the kitchen. Although the food menu order is obtained from a guest waiter, the order 
    can be done simultaneously by several chefs in the kitchen so that the processing time 
    for the food order menu can be done at the same time / without waiting time for each order.