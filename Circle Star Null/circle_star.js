// Write a function which, taking in a positive integer n as input, returns a string “CIRCLE” if the n is divisible
// by 2, a string “STAR” if the n is divisible by 3, a string “CIRCLE STAR” if n is divisible by 2 and 3, and return
// null if n is divisible by neither 2 and 3.
// sample expected output:
// getShape(5); ⇒ Null
// getShape(6); ⇒ CIRCLE STAR
// getShape(9); ⇒ STAR

function getShape(number){

    switch(true) {
        case (number%2===0 && number%3===0) :
            return "CIRCLE STAR"  
        case (number%2===1 && number%3!==0) :
            return "Null"
        case (number%2===0) :
            return "CIRCLE" 
        case (number%3===0) :
            return "STAR"
    }
}
const shape = getShape(1)
console.log(shape)
