// In the language of your choice, write a function which, taking a positive integer n as input, finds all sets
// of numbers that sum up to n.
// For example, n=4, we have: 4
// 3,1
// 2,2
// 2,1,1
// 1,1,1,1
// Note that 2,1,1 is same as 1,2,1 or 1,1,2


function printVector(result)
{
    if (result.length != 1) {
 
        for (var i = 0; i < result.length; i++) {
            process.stdout.write( result[i] + " ");
        }
        console.log("")
    }
}
 
function findWays(result, counter, n)
{
    if (n == 0)
        printVector(result);
 
    for (var j = counter; j <= n; j++) {

        result.push(j);
        findWays(result, j, n - j);
        result.pop();
    }
}
 

var n = 4;
var result = [];
findWays(result, 1, n);