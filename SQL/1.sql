-- 1) Provide a query select statement to return the following result structure:
-- name; total_price
-- Where name is the name of “persons” and total_price is the total of price from the items transaction
-- completed by the person.

query = `   SELECT p.name, SUM(t.total * CAST(coalesce(i.price , '0') AS integer)) as total_price
            FROM transactions t
            INNER JOIN 
                persons p on p.id = t.person_id
            INNER JOIN 
                items i on i.id  = t.item_id 
            GROUP BY p.name
        `
