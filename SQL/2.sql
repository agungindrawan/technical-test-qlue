-- 2) Provide a query select statement to return the transaction complete with name of person, and name
-- of item between January 1st, 2020 and June 25th, 2020.

query  =   `SELECT p.name, i.name as name_of_item, t.date
            FROM transactions t
            INNER JOIN 
                persons p on p.id = t.person_id
            INNER JOIN 
                items i on i.id  = t.item_id 
            WHWERE 
                t.date >='2020-01-01' 
            AND 
                t.date <= '2020-06-25'`
