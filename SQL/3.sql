-- 3) Provide a query select statement to return a list of the name of the person who made transactions
-- based on the total variety of items purchased, sorted from the most to the fewest.

query =`SELECT p.name, t.total 
        FROM transactions t
        INNER JOIN 
                persons p on p.id = t.person_id
        INNER JOIN 
                items i on i.id  = t.item_id 
        ORDER BY t.total desc`